package com.songoda.ultimatetimber.treefall;

import com.songoda.ultimatetimber.UltimateTimber;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class FakeTreeDetection implements Listener {

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        if (!TreeChecker.validMaterials.contains(e.getBlockPlaced().getType())) return;

        e.getBlockPlaced().setMetadata("fakeTree", new FixedMetadataValue(UltimateTimber.getInstance(), true));
    }

}
